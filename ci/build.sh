#!/bin/sh -e

if test -e env/bin/activate
then
    . ./env/bin/activate
fi

rm -fr dist
./setup.py sdist

docker build .
