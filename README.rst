============
Auth Service
============

Auth service, it verifies user credentials and returns auth token, also validates auth token

:Repository:    `<https://gitlab.com/microservices5894911/python_tornado/video_converter/auth>`_
:Docker Tags:   `<https://hub.docker.com/repository/docker/mvijayaragavan/auth/>`_


Environment Variables
=====================

Required
--------
- ``PGSQL_APP``: Connection string used to connect to the app database.
- ``JWT_SECRET``: JWT secret.


Quickstart Development Guide
============================

Setting up your environment
---------------------------
.. code-block:: bash

   $ python3.9 -m venv env
   $ . ./env/bin/activate
   $ pip install -e '.[docs,tests]'
   $ ./bootstrap.sh
   $ . ./build/test-environment

Testing the application in dev
------------------------------
.. code-block:: bash

	(env) $ flake8
	(env) $ coverage run && coverage report

Running Locally
---------------
.. code-block:: bash

	(env) $ auth

Building
--------
.. code-block:: bash

	(env) $ docker build .

Using in Another Development Environment
----------------------------------------
This service publishes a docker image to `https://hub.docker.com/repository/docker/mvijayaragavan/auth/`_ so
integrating it into an existing docker-based development environment is simple.

At the time of writing the following docker-compose snippet is what is required:

.. code-block:: yaml

    %YAML 1.2
    ---
    version: "3.0"
    services:
      appdb:
        image: postgres:14.7-alpine
        ports:
          - 5432
        environment:
          PGUSER: postgres
          POSTGRES_PASSWORD: pgpassword
          POSTGRES_DB: app

      auth:
        image: mvijayaragavan/auth:latest
        ports:
          - 8000
        environment:
          PGSQL_APP: postgresql://postgres:pgpassword@appdb:5432/app
          JWT_SECRET: yJzb21lIjoicGF5bG9hZCJ9
