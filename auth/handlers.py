import datetime
import os

import bcrypt
import jwt
import pkg_resources

from auth import helpers, version


class OpenApiHandler(helpers.RequestHandler):
    """Services up the index page and OpenAPI doc"""
    def get(self, *args, **kwargs):
        template = args[0] if args else "index.html"
        if args and args[0] == "openapi.yaml":
            self.set_header("Content-Type", "text/yaml")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.render(
            template,
            **{
                "host": self.request.host,
                "scheme": self.request.protocol,
                "settings": self.settings,
                "version": pkg_resources.get_distribution(__package__).version,
                "name": "Auth Service",
            },
        )


class StatusHandler(helpers.RequestHandler):
    """
    Simple heartbeat handler.

    Doesn't check anything beyond IOLoop responsiveness.

    """
    def get(self):
        print(self.application)
        if self.application.ready_to_serve:
            status = 200
            text = 'ok'
        else:
            status = 503
            text = 'maintenance'
        self.set_status(status)
        self.send_response({
            'service': self.settings['service'],
            'version': version,
            'status': text
        })


class LoginHandler(helpers.PostgresMixin, helpers.RequestHandler):
    async def post(self):
        body = self.get_request_body()

        try:
            user_name = body['user_name']
            password = body['password']
        except KeyError:
            self.send_error(403, reason='Missing credentials')
            return

        users = await self.query_appdb('login',
                                       'SELECT user_name, password, role '
                                       'FROM users JOIN roles '
                                       'ON users.role_id = roles.id '
                                       'WHERE user_name=%(user_name)s',
                                       user_name=user_name,
                                       password=password)
        if users:
            user = users[0]

            if bcrypt.checkpw(password.encode('utf-8'),
                              user['password'].encode('utf-8')):
                jwt_token = self.createJWT(user_name,
                                           os.environ.get("JWT_SECRET"),
                                           user['role'])
                self.send_response({'token': jwt_token})
            else:
                self.send_error(401, reason='Invalid credentials')
        else:
            self.send_error(401, reason='Invalid credentials')

    def createJWT(self, user_name, secret, role):
        return jwt.encode(
            {
                "username": user_name,
                "exp": datetime.datetime.now(tz=datetime.timezone.utc) +
                datetime.timedelta(days=2),
                "iat": datetime.datetime.utcnow(),
                "role": role,
            },
            secret,
            algorithm="HS256",
        )


class TokenValidateHandler(helpers.PostgresMixin, helpers.RequestHandler):
    async def get(self):
        if 'Authorization' not in self.request.headers:
            self.send_error(403, reason='Missing token')
            return

        encoded_jwt = self.request.headers['Authorization'].split()

        if len(encoded_jwt) > 1:
            authrization_token = encoded_jwt[1]
            try:
                decoded = jwt.decode(authrization_token,
                                     os.environ.get("JWT_SECRET"),
                                     algorithms=["HS256"])
                self.send_response(decoded)
            except jwt.ExpiredSignatureError:
                self.send_error(401, reason='Token expired')
                return
            except jwt.PyJWTError:
                self.send_error(401, reason='Invalid token')
                return
        else:
            self.send_error(403, reason='Missing token')
