import logging
import uuid

import psycopg2.errorcodes
import sprockets.http.mixins
from sprockets.mixins.mediatype import content
from tornado import web


class RequestHandler(content.ContentMixin, sprockets.http.mixins.ErrorLogger,
                     sprockets.http.mixins.ErrorWriter, web.RequestHandler):
    """Basic request handler.

      This is the base class for all request handlers.  It contains
      functionality and behavior that is available everywhere.

    """
    access_log_failures_only = True

    def __init__(self, *args, **kwargs):
        # this needs to be set BEFORE super.__init__ since it WILL
        # BE referred to from within set_default_headers during
        # super().__init()
        self.correlation_id = str(uuid.uuid4())
        super().__init__(*args, **kwargs)
        self.logger = logging.getLogger(self.__class__.__name__)


class PostgresMixin:
    """Mixin that provides asynchronous postgres access functionality"""
    async def query_appdb(self, operation_name, query, *args, **params):
        """
        Asynchronously query AppDB.

        :param str operation_name: informational name of the operation
            that you are performing.  This is used for logging and
            metric generation.
        :param str query: psycopg2-ready query or stored-procedure name
        :param args: unnamed args will be passed into a stored-procedure
        :param params: dictionary of parameters to substitute into `sql`
        :keyword set expose_error_codes:
        :return: :class:`list` of result rows as :class:`dict` instances
        :raises: :exc:`web.Finish` if a database error occurs

        """
        # exposed_error_codes = params.pop('expose_error_codes', set())
        timeout = self.application.settings['aiopg_execute_timeout']
        try:
            async with self.application.appdb.acquire() as conn:
                async with conn.cursor(
                        cursor_factory=psycopg2.extras.DictCursor) as cur:
                    await cur.execute(query, params, timeout=timeout)
                    return await cur.fetchall()
        except psycopg2.Error as exc:
            # if exc.pgcode in exposed_error_codes:
            #     raise

            self.logger.exception(
                'database failure during %s: pgcode=%r pgerror=%r exc=%r',
                operation_name, exc.pgcode, exc.pgerror, exc)
            if isinstance(exc, psycopg2.OperationalError):
                self.send_error(503, reason='AppDB Connection Failure')
            else:
                self.send_error(500, reason='Database Failure')
            raise web.Finish()
        except Exception as exc:
            self.logger.exception(
                'Unexpected failure during %s: query=%r params=%r exc=%r',
                operation_name, query, params, exc)
            self.send_error(500, reason='Database Failure')
