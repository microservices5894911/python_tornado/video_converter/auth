#!/usr/bin/env sh

set -ex

test -n "$SHELLDEBUG" && set -x

TEST_HOST="${TEST_HOST:-127.0.0.1}"

get_exposed_port() {
  docker-compose port "$@" | cut -d: -f2
}

wait_for() {
  echo "Waiting for $*"
  wait-for -t 60 -s 5 "$@"
}

rm -rf build
mkdir build

docker-compose pull appdb
docker-compose down --timeout 0 --remove-orphans --volumes

docker-compose up -d appdb

wait_for "postgresql://postgres:pgpassword@${TEST_HOST}:$(get_exposed_port appdb 5432)/app"

PGSQL_APP=postgresql://postgres:pgpassword@$TEST_HOST:$(get_exposed_port appdb 5432)/app \
  prep-it

echo Environment variables:
tee build/test-environment << EOF
export PGSQL_APP=postgresql://postgres:pgpassword@${TEST_HOST}:$(get_exposed_port appdb 5432)/app
export JWT_SECRET=yJzb21lIjoicGF5bG9hZCJ9
EOF

. ./build/test-environment

echo Bootstrap complete
