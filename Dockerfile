FROM mvijayaragavan/python3:3.9.16-0 AS service
LABEL url="https://gitlab.com/microservices5894911/python_tornado/video_converter/auth"

ARG PSYCOPG_VERSION=3.1.4
ARG PSYCOPG2_VERSION=2.9.5

ENV SERVICE=auth
COPY dist/*.tar.gz /tmp/
EXPOSE 8000
RUN apk --no-cache add libpq \
 && apk add --no-cache --virtual .build gcc linux-headers musl-dev postgresql-dev \
 && pip install --no-cache-dir --requirement /tmp/installed-packages.txt \
 && pip install --no-cache-dir /tmp/*.tar.gz \
 && pip3 install "psycopg[binary,pool]==${PSYCOPG_VERSION}" psycopg2==${PSYCOPG2_VERSION} psycopg2-binary==${PSYCOPG2_VERSION} \
 && apk del --purge .build \
 && rm -fr /var/cache/apk/*
CMD "${SERVICE}"
